\documentclass{article}

\author{Benjamin Alheit}
\date{\today}
\title{Summary of some literature}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage[margin = 0.7in]{geometry}
\usepackage{enumitem}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{bm}
\usepackage{float}
\usepackage{subfig}
\usepackage[backend=biber]{biblatex}
\usepackage{color}
\usepackage{siunitx}
\bibliography{lit_sum.bib}

\begin{document}
\maketitle

\section{Mechanical properties of collagen}

\subsection{Tensile Mechanical Properties of Three-Dimensional Type I Collagen Extracellular Matrices With Varied Microstructure \cite{collagen_gel_tensile}}

In this article the researchers prepare a collagen gel that has concentrations of type I collagen of 0.3, 1.0, 2.0 and 3.0 mg/mL. These are typical values for collagen gels found in vitro (created and tested in labs). Their purpose of investigating this is that similar gels are used in clinical applications as tissure substitutes. Connective tissues in vivo (in living organisms), however, typically have concentrations of 30-40 mg/mL. After scouring the article I could not find what the remainder of the gel consisted of. I can only assume that they assumed that the other material had no mechanical strength in comparison to the collagen. The collagen fibrils are randomly orientated in a 3-D sense in the gel. A image of a characteristic stress-strain curve is shown in the figure \ref{fig:gel_ss}.
Interesting results that they found are:
\begin{itemize}
\item Failure stress and linear modulus increased with an increase in collagen concentration
\item Failure strain did not seem to be a function of collagen concentration
\end{itemize}

\begin{figure}
\centering
\includegraphics[width = 0.7\linewidth]{col_stress_strain}
\caption{Screenshot from \cite{collagen_gel_tensile}}
\label{fig:gel_ss}
\end{figure}


\subsection{The Molecular and Fibrillar Structure of Collagen and its Relationship to the Mechanical Properties of Connective Tissue \cite{Parry_col_mech}}

\textcolor{red}{Get this}


\subsection{Stress-strain Experiments on Individual Collagen Fibrils \cite{Indv_col}}

\textcolor{red}{Note: references 14-18 for collagen modelling,\\ 
24-25 for mechanical testing of clumps of fibrils} 

``... the stress-strain curve of collagen fibrils is dictated by cross-link density. Highly cross-linked fibrils behave like hardening springs that rupture in a brittle manner at their peak stress, whereas low-density cross-linking results in energy dissipating postpeak deformations and `graceful' failure."\\
\\
In this article stress-strain data was generated for single fibrils of collagen at a time. This was done using a microelectromechanical systems (MEMS) technique. 13 different fibrils were tested. Cyclic loading tests were done for four fibrils, the results of which are shown in figure \ref{fig:cyclic_fibril}. Each fibril was loaded and unloaded 4 times. Lines of best fit were fitted to the loading and unloading cycles. The stress-strain relationship for the first loading of each fibril was unique where as the remaining three coincided with each other within the experimental uncertainty. Therefore, the first loading was assigned its own line of fit, whereas one line of fit was assigned for the other three loadings. Similarly, all unloading stress-strain curves were undistinguishable beyond experimental uncertainty and hence were all assigned the same line of fit. The results of each fibril varies. The authors of the paper say that this is likely to be due to different amounts of cross-linking in each fibril, although they were not able to determine the amount of cross-linking in each fibril. However, it is important to note that all tests are done to different levels of strain (as a limitation of the apparatus that was being used), making the qualitative response appear more different than they actually are. For instance all fibrils seem to go non-linear at a strain of $ \sim $\SI{30}{\%}\\
\\
The authors also found that there was a relationship between yield strength and volume of the fibril for the tests that they ran. Hence, they concluded that the difference in responses was partially due to a size effect and that the material would not respond as a homogeneous material at this level. 

\begin{figure}
\centering
\includegraphics[width=\linewidth]{cyclic_fibril}
\caption{Screenshot of figure from \cite{Indv_col}}
\label{fig:cyclic_fibril}
\end{figure}



\subsection{Strain-rate sensitive mechanical properties of tendon fascicles from mice with genetically engineered alterations in collagen and decorin \cite{col_mice}}

\textcolor{red}{Note: references [5,6,18,24,25] for tendon fascicles and collagen fiber testing} 


``It is generally thought that type I collagen provides the primary elastic strength and stiffness to tendon. Parry [7] observed increased strength with increasing collagen content in rat tail tendon and Mikic et al. [8] found decreased failure loads with
decreased collagen content in mouse Achilles tendons''\\
\\
``Collagen alone has not been widely implicated in tendon viscoelasticity. Therefore, we hypothesized that tendon tissue with altered type I collagen content will have altered elastic properties but similar viscoelastic properties compared to normal control tendons.''\\
\\
``Differences in tendon elastic failure properties and viscoelastic properties, such as strain rate sensitivity of mechanical properties, may correlate with decorin content. Due to evidence that collagen fibrils are discontinuous [11,12], it has been postulated that non-collagenous matrix proteins provide inter-fibrillar links for force transmission. In particular, proteoglycans (PGs) with their associated glycosaminoglycan (GAG) chains may have an important role in this function [11,13--16]. Flint et al. [17] saw correlations between GAG content and ``functional differences'' in skin. More recently, Pins et al. [18] produced stronger collagen fibers in vitro with the addition of the PG decorin.''\\
\\
The authors tested multiple fascicles of mouse tail tendons. They hypothesise that changes in collagen and decorin content will affect the mechanical response of the tendon. To test this, they grew a range of mice with transgenics, one that would create more type I collagen, one that would reduce the amount of type I collagen. To see the effect of decorin a set of mice were bred using a ``decorin knock-out mouse'' that would disrupt the decorin gene and prevent decorin from occurring in the offspring. Every test group was tested at both a slow (0.5\%/s) and fast (50\%/s) strain rate. A summary of the different test groups is shown in figure \ref{fig:mice_groups}.
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{mice_groups}
\caption{Screeenshot from \cite{col_mice}}
\label{fig:mice_groups}
\end{figure}
\\
\\
A characteristic stress-strain curve is shown in figure \ref{fig:mice_stress_strain}.
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{mice_stress_strain}
\caption{Screeenshot from \cite{col_mice}. Reported to be `typical' non-linear stress-strain behaviour for soft tissue. Qualitatively similar to \ref{fig:gel_ss}}
\label{fig:mice_stress_strain}
\end{figure}
The values for maximum stress and modulus vary only marginally among test groups for the test groups when tested at the slow strain rate. The maximum stress and modulus are approx 28 MPa and 410 GPa, respectively. However, the variations among test groups are a lot larger for the high strain rate tests as shown in figure \ref{fig:mice_fast}.
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{mice_fast}
\caption{Screeenshot from \cite{col_mice}.}
\label{fig:mice_fast}
\end{figure}
Figure \ref{fig:mice_fast} implies that the amount of decorin in the tissue does in fact have a significant effect on the time dependence of the mechanical properties of the tissue. This is reiterated in figure \ref{fig:mice_time_dependence}.
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{mice_time_dependence}
\caption{Screeenshot from \cite{col_mice}.}
\label{fig:mice_time_dependence}
\end{figure}

\subsection{Questions that must still be answered}
\begin{itemize}
\item All experimental data looked at so far looks at the mechanical properties in the fiber/fibril direction. What about the anisotropy of the material?
\item \cite{col_mice} shows us that soft collagenous materials are (or can be) strain-rate dependent, but how do we model this strain-rate dependence in the context of this particular stress-strain relationship?
\item V V NB what is the exact content of the soft tissue in the turtle shell (amount proteoglycans and their associated glycosaminoglycan chains, concentration of collagen, and whatever else)
\end{itemize}

\section{Structure of the shell}
As a matter of terminology - the top part of the turtle shell is referred to as a carapace and the bottom part of the turtle shell is referred to as the plastron. The turtle shell consists of an bone base structure that is covered by a layer of keratin\cite{ScutesMoustakas-Verho}. The bone base structure consists of a spine as can be seen in figure \ref{fig:bone_base}. The spine has `modified rids' protruding from it. Each rib is associated with one bone plate which interlocks with the adjacent plates in the form of a suture that is filled with a highly collagenous material, binding the adjacent plates.
\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{bone_structure}
\caption{Image of a turtle skeleton from beneath}
\label{fig:bone_base}
\end{figure}
\noindent
The keratin layer is also divided into plates known as scutes. However, the bone plates and the keratin plates do not match each other. This is evident from the scars the boundaries of the keratin plates have left on the bone of the shell in image \ref{fig:shells} \ref{fig:dry_shell}. Since these scars do not coincide with the sutures of the bone plates it is clear that the keratin plates do not coincide with the bone plates. 
\begin{figure}[H]
  \centering
    \subfloat[Turtule shell with keratin layer]{\includegraphics[width=0.6\textwidth]{live_shell}\label{fig:live_shell}}
 \hfill
  \subfloat[Bone of tortoise shell]{\includegraphics[width=0.25\textwidth]{dry_shell}\label{fig:dry_shell}}

  \caption{Images of turtle shells with and without keratin layers respectively}
  \label{fig:shells}
\end{figure}

\subsection{Internal bone morphology}
The morphology of the bone plate changes through the thickness of the bone. This is displayed in an image from \cite{MechPropertiesAcharai} shown in figure \ref{fig:bone_morph} below.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{bone_morph}
\caption{Taken from \cite{MechPropertiesAcharai}}
\label{fig:bone_morph}
\end{figure}

\subsection{Bone plate - suture morphology}
The overall bone plate-suture morphology can be well understood by looking at the image in figure \ref{fig:bone-suture} below. 

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{bone_suture}
\caption{Taken from \cite{MechPropertiesAcharai}}
\label{fig:bone-suture}
\end{figure}


\pagebreak
\section{Mechanical Properties of the shell}
Mechanical properties of multiple components of the shell are provided by \cite{MechPropertiesAcharai}. Some of their results are shown in figure \ref{fig:mech_prop}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{mech_properties}
\caption{Taken from \cite{MechPropertiesAcharai}}
\label{fig:mech_prop}
\end{figure}

The mechanical properties and different anisotropic natures of each component of the shell are also displayed in figure \ref{fig:ani} below.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{anisotropic}
\caption{Taken from \cite{MechPropertiesAcharai}}
\label{fig:ani}
\end{figure}

Similar tests have been done by \cite{MechPropRhee} although not as extensive. The tests done by \cite{MechPropRhee} were also done by means of a quasi static compression test as opposed to the nanoindentation tests done by \cite{MechPropertiesAcharai}. Their results roughly coincide in terms of Young's modulus however \cite{MechPropRhee} also provides various stress strain curves shown in figure \ref{fig:stress_strain} below.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{stress_strain}
\caption{Taken from \cite{MechPropRhee}}
\label{fig:stress_strain}
\end{figure}

It is interesting to note the highly non-linear behaviour in the stress-strain curves of the complete bone thickness. It is also interesting to note that it becomes increasingly stiffer with a larger strain rate. Three point bending tests have also been done by \cite{MechPropRhee} and compared with a FEM simulation that was created using the tested mechanical properties. This is shown in figure \ref{fig:bend_test} below.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{bend_test}
\caption{Taken from \cite{MechPropRhee}}
\label{fig:bend_test}
\end{figure}

Tests done on the keratin layer have also been performed by \cite{keratinProp}. Their tests were done by means of quasi-static tensile tests as opposed to the nanoindentation tests done by \cite{MechPropertiesAcharai}. The results are shown in figure \ref{fig:keratin_test} below. 

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{keratin_tests}
\caption{Taken from \cite{keratinProp}}
\label{fig:keratin_test}
\end{figure}

Again, the results roughly coincide with that of \cite{MechPropertiesAcharai}.

\section{Mechanical benefits of sutures}

``This outstanding performance is possible by their internal architectures, which can be described as assemblies of stiff and strong building blocks (collagen or chitin fibers, mineral tablets) (Barthelat, 2015) bonded by weaker interfaces. These interfaces are as important as the building blocks and fulfill critical structural functions (Barthelat et al., 2016): They deflect and channel cracks into toughening configurations (He et al., 1994; Khayer Dastjerdi et al., 2013), they enable large deformations (Smith et al., 1999) and they provide deformation hardening to spread energy dissipative mechanisms throughout large volumes of the materials (Barthelat, 2015; Barthelat et al., 2016; Dunlop et al., 2011; Fratzl et al., 2004; Fratzl et al., 2016; Meyers et al., 2008).'' - \cite{bioInterlocking}.\\
\\
``A numerical study of a lizard skull with fused and unfused sutures found that suture
interfaces can reduce local strain by distributing strain around the skull (Moazen et al., 2009).'' - \cite{lizSkull}. Sutures decrease the strain energy density by dispersing the strain energy throughout the material.

\printbibliography
\end{document}
