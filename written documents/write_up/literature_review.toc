\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\contentsline {section}{\numberline {1}Bio-materials}{2}
\contentsline {subsection}{\numberline {1.1}Bone}{2}
\contentsline {subsubsection}{\numberline {1.1.1}Bone structure}{2}
\contentsline {subsubsection}{\numberline {1.1.2}Bone mechanics and modelling}{2}
\contentsline {subsection}{\numberline {1.2}Collagen}{3}
\contentsline {subsection}{\numberline {1.3}Keratin}{3}
\contentsline {section}{\numberline {2}Shell structure}{4}
\contentsline {subsection}{\numberline {2.1}Macrostructures}{4}
\contentsline {subsubsection}{\numberline {2.1.1}Skeletal structure}{4}
\contentsline {subsubsection}{\numberline {2.1.2}Keratinous structure}{4}
\contentsline {subsection}{\numberline {2.2}Constitute components}{5}
\contentsline {subsubsection}{\numberline {2.2.1}Internal bone morphology (Pleural)}{5}
\contentsline {subsubsection}{\numberline {2.2.2}Collagenous sutures}{5}
\contentsline {subsubsection}{\numberline {2.2.3}Keratin scutes}{6}
\contentsline {section}{\numberline {3}Collagen}{7}
\contentsline {section}{References}{7}
