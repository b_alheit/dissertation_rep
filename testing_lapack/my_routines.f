      subroutine myPrint(A, n, m)
      integer i, j, n ,m
      double precision A(n,m)
      do i=1,n
        do j=1,m
          write(*, fmt="(1x,a,i0)", advance="no") A(j,j), " "
        enddo
        print *, ""
      enddo

      end subroutine
