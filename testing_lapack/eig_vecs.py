import numpy as np
import scipy as sp

A = np.array([[3.1, 1.3, -5.7],
              [1.0, -6.9, 5.8],
              [3.4, 7.2, -8.8]])

A = A+A.T

B1, B2 = np.linalg.eig(A)

print(A)
print()
print(B1)
print()
print(B2)
print()
print(np.matmul(B2[:, 1].T, np.matmul(A, B2[:, 1])))
