import numpy as np
import matplotlib.pyplot as plt

stretch = np.linspace(1, 1.6, 500)

c1 = 0.18582
# c2 = 100*0.20583
c2 = -0.20583

sig = lambda lam, c1, c2: 2*(lam**2 - 1/lam) * (c1 + c2/lam)

stress = sig(stretch, c1, c2)

plt.plot(stretch, stress)
plt.show()
