import numpy as np
import matplotlib.pyplot as plt

stretch = np.linspace(1, 1.6, 500)

c1 = 0.18582
# c2 = 100*0.20583
c2 = -0.20583
# c = [-0.48953, 1.30073, 0.73743, 2.95646, -0.60229, 1.35266]
c = [0.005044, 5.7255, 0.005044, 5.7255, 0.005044, 5.7255]
c = [0.002522, 5.7255] *3
c = [0.005044, 5.7255**(2/1)] *3

def sig(lam):
    out = 0
    for i in range(int(len(c)/2)):
        out += c[2*i] * (lam ** c[2*i+1] - lam ** (-c[2*i+1]/2))
        # out += c[2*i] * (lam ** c[2*i+1] - (2**(-1+c[2*i+1])) * lam ** (-c[2*i+1]/2))
    return out

def sig2(lam):
    out = 0
    for i in range(int(len(c)/2)):
        out += c[2*i] * (lam ** c[2*i+1] - lam ** (-c[2*i+1]))
    return out

c1 = 0.004154
# c2 = 100*0.20583
c2 = 0.050753
c3 = -0.013199

I1 = lambda lam: lam**2 + 2/lam
I2 = lambda lam: 2*lam + 1/(lam**2)
I3 = lambda lam: 1

sigYeoh = lambda lam, c1, c2, c3: 2*(lam**2 - 1/lam) * (c1 + 2*c2*(I1(lam)-3) + 3*c3*(I1(lam)-3)**2)

# sig = lambda lam, c1, c2: 2*(lam**2 - 1/lam) * (c1 + c2/lam)

stress = sig(stretch)
stressYeoh = sigYeoh(stretch, c1, c2, c3)

plt.plot(stretch, stress, label="ogden")
plt.plot(stretch, stressYeoh, label="Yeoh")
plt.legend()
plt.grid()
plt.show()
