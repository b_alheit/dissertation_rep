import numpy as np
import matplotlib.pyplot as plt

stretch = np.linspace(1, 1.6, 500)

c1 = 0.004154
# c2 = 100*0.20583
c2 = 0.050753
c3 = -0.013199

I1 = lambda lam: lam**2 + 2/lam
I2 = lambda lam: 2*lam + 1/(lam**2)
I3 = lambda lam: 1

sig = lambda lam, c1, c2, c3: 2*(lam**2 - 1/lam) * (c1 + 2*c2*(I1(lam)-3) + 3*c3*(I1(lam)-3)**2)

stress = sig(stretch, c1, c2, c3)

plt.plot(stretch, stress)
plt.show()
